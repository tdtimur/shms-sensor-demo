import { writable } from 'svelte/store';
import { browser } from '$app/environment';

type Theme = 'light' | 'dark';
export const themeStore = browser
	? writable<Theme>((localStorage.getItem('theme') as Theme) ?? 'light')
	: writable<Theme>('light');

themeStore.subscribe((value) => {
	if (browser) {
		localStorage.theme = value;
	}
});
