import { writable } from 'svelte/store';
import { browser } from '$app/environment';

export const loginStore = browser
	? writable<boolean>((localStorage.getItem('loggedIn') ?? 'false') === 'true')
	: writable<boolean>(false);

export const userStore = browser
	? writable<string>(localStorage.getItem('username') ?? '')
	: writable<string>('');

loginStore.subscribe((value) => {
	if (browser) localStorage.loggedIn = value.toString();
});

userStore.subscribe((value) => {
	if (browser) localStorage.username = value;
});
